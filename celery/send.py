#!/usr/bin/env python3

from tasks import add

result = add.delay(4, 4)
assert result.get(timeout=10) == 8
print("[*] Result received")

import gridfs
from pymongo import MongoClient

db_name = "test-database"
uri = f"mongodb://test-user:test-password@mongodb-srv:27017/{db_name}"
db = MongoClient(uri)[db_name]
fs = gridfs.GridFS(db)
print(fs.list())
meta = dict(username="AAA", tags=["top-secrete"])
index = fs.put(b"hello world", filename="foo", metadata=meta)
out = fs.get(index)
assert out.metadata == meta
assert "md5sum" not in out.metadata

from tasks import calc_md5sum
from gridfs.grid_file import ObjectId

result = calc_md5sum.delay(str(index))
index2 = result.get(timeout=10)
out2 = fs.get(ObjectId(index2))
assert out2.metadata["md5sum"] == '5eb63bbbe01eeed093cb22bb8f5acdc3'
print(out2.metadata)
