#!/usr/bin/env python3

from celery import Celery

app = Celery("tasks", backend="redis://redis-srv:6379", broker="redis://redis-srv:6379")


@app.task
def add(x: int, y: int) -> int:
    print("[*] Result send")
    return x + y


db_name = "test-database"
uri = f"mongodb://test-user:test-password@mongodb-srv:27017/{db_name}"


@app.task
def calc_md5sum(index: str):
    import hashlib
    import io

    import gridfs
    from gridfs.grid_file import ObjectId
    from pymongo import MongoClient

    db = MongoClient(uri)[db_name]
    fs = gridfs.GridFS(db)
    file = fs.get(ObjectId(index))
    contents = file.read()
    file.metadata["md5sum"] = hashlib.md5(contents).hexdigest()
    filename = file.filename
    meta = file.metadata
    index = fs.put(contents, filename=filename, metadata=meta)

    return str(index)
