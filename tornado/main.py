#!/usr/bin/env python

"""FastAPI Server implementation"""

from contextlib import contextmanager
from typing import Dict

import celery
import gridfs.grid_file
from fastapi import FastAPI, File, HTTPException, UploadFile
from gridfs.grid_file import ObjectId
from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorGridFSBucket
from pydantic import BaseModel

import tasks

route = app = FastAPI()

DB_NAME = "test-database"
MONGO_URI = f"mongodb://test-user:test-password@mongodb-srv:27017/{DB_NAME}"


@contextmanager
def fs_bucket() -> AsyncIOMotorGridFSBucket:
    """Encapsulates the proper construction of GirdFS Async instances (stack only)"""
    client = AsyncIOMotorClient(MONGO_URI)
    yield AsyncIOMotorGridFSBucket(client[DB_NAME])


async def delete4mongo(filename: str, bucket: AsyncIOMotorGridFSBucket) -> int:
    """Deletes all the files with the given name from MongoDB
    and returns how much"""
    count = 1
    cursor = bucket.find({"filename": filename})
    async for grid_out in cursor:
        bucket.delete(grid_out._id)  # pylint: disable=protected-access
        count += 1
    return count


@route.post("/files")
async def create_upload_file(file: UploadFile = File(None)):
    """Uploads the given file"""
    with fs_bucket() as bucket:
        await delete4mongo(file.filename, bucket)
        # https://www.starlette.io/requests/
        metadata = dict(content_type=file.content_type)
        file_id = await bucket.upload_from_stream(
            file.filename, file.file, metadata=metadata
        )
        return {"file_id": str(file_id)}


def summary(out: gridfs.grid_file.GridOut) -> Dict:
    """Collects a summary for the given GridFS entity"""
    return dict(
        name=out.name,
        content_type=out.content_type,
        length=out.length,
        md5=out.md5,
        upload_date=f"{out.upload_date:%Y-%m-%d %H:%M:%S}",
        metadata=out.metadata,
    )


@route.get("/files")
async def list_existing_files(details: bool = False):
    """List of existing file ids"""
    files = {}
    with fs_bucket() as bucket:
        cursor = bucket.find()
        async for out in cursor:
            file_id = str(out._id)  # pylint: disable=protected-access
            files[file_id] = summary(out)

    if not details:
        return list(files.keys())

    return files


@route.get("/files/{file_id}")
async def get_existing_file(file_id: str):
    """Get summary for the given file_id"""
    with fs_bucket() as bucket:
        _id = ObjectId(file_id)
        out = await bucket.find(dict(_id=_id)).next()
        meta = summary(out)
        return meta


@route.get("/file_id/{filename}")
async def get_filename_2_id(filename: str):
    """Get file_id for the given filename, if exists"""
    with fs_bucket() as bucket:
        cursor = bucket.find({"filename": filename})
        async for grid_out in cursor:
            file_id = str(grid_out._id)  # pylint: disable=protected-access
            return dict(file_id=file_id)

    raise HTTPException(status_code=404, detail="Item not found")


@route.delete("/files/{file_id}")
async def remove_existing_file(file_id: str):
    """Remove file with the given file_id"""
    with fs_bucket() as bucket:
        _id = ObjectId(file_id)
        cursor = bucket.find(dict(_id=_id))
        async for grid_out in cursor:
            filename = grid_out.name
            await bucket.delete(grid_out._id)  # pylint: disable=protected-access
            return dict(filename=filename)

    raise HTTPException(status_code=404, detail="Item not found")


class Rename(BaseModel):  # pylint: disable=missing-class-docstring
    newname: str


@route.put("/rename/{file_id}")
async def rename_existing_file(file_id: str, item: Rename):
    """Rename file with the given file_id"""
    try:
        with fs_bucket() as bucket:
            _id = ObjectId(file_id)
            await bucket.rename(_id, item.newname)
            return dict(filename=item.newname)
    except BaseException as exc:
        # pylint: disable=raise-missing-from
        raise HTTPException(status_code=404, detail=str(exc))


@route.put("/md5sum/{file_id}")
async def run_md5sum(file_id: str):
    """Launches MD5SUM calculation for the previously loaded file with the given name"""
    result = tasks.update_md5sum.delay(file_id)
    return dict(md5sum_id=result.id)


@route.get("/status/{md5sum_id}")
async def status_md5sum(md5sum_id: str):
    """Gets status on MD5SUM calculation for the given Celery result ID"""
    result = celery.result.AsyncResult(md5sum_id)
    return dict(status=result.status)
