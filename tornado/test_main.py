#!/usr/bin/env python

"""Tests for FastAPI application"""

import io
from collections import namedtuple
from contextlib import asynccontextmanager

import celery
import pytest
from gridfs.grid_file import ObjectId
from httpx import AsyncClient

import main
import tasks

# pylint: disable=redefined-outer-name,missing-function-docstring

File = namedtuple("File", ["name", "contents", "wrapper"])


@pytest.mark.asyncio
async def test_async_client():
    async with AsyncClient() as client:
        response = await client.get("https://www.example.com/")
        assert response.status_code == 200


@pytest.fixture
def file() -> File:
    name = "dummy"
    contents = b"dummy"
    wrapper = io.BytesIO(contents)
    yield File(name, contents, wrapper)


@pytest.fixture
def files(file) -> File:
    yield {"file": (file.name, file.wrapper, None)}


@pytest.fixture
async def client():
    async with AsyncClient(
        app=main.app, base_url="http://localhost:8000"
    ) as async_client:
        yield async_client


@asynccontextmanager
async def run(engine, *args, **kwds):
    resource = await engine(*args, **kwds)
    try:
        yield resource
    finally:
        pass


@pytest.mark.asyncio
async def test_create_upload_file(client, files):
    file_id = ""
    async with run(client.post, "/files", files=files) as response:
        assert response.status_code == 200
        data = response.json()
        file_id = data["file_id"]
        assert file_id != ""

    async with run(client.post, "/files", files=files) as response:
        assert response.status_code == 200
        data = response.json()
        assert data["file_id"] != file_id
        file_id = data["file_id"]

    async with run(client.get, "/files") as response:
        assert response.status_code == 200
        data = response.json()
        assert file_id in data

    filename = files["file"][0]
    async with run(client.get, "/files/?details=true") as response:
        assert response.status_code == 200
        data = response.json()
        assert file_id in data
        file = data[file_id]
        assert file["name"] == filename

    async with run(client.get, f"/files/{file_id}") as response:
        assert response.status_code == 200
        data = response.json()
        assert data["name"] == filename

    newname = filename.swapcase()
    async with run(
        client.put, f"/rename/{file_id}", json=dict(newname=newname)
    ) as response:
        assert response.status_code == 200
        data = response.json()
        assert data["filename"] == newname

    dummy_id = str(ObjectId())
    async with run(
        client.put, f"/rename/{dummy_id}", json=dict(newname=newname)
    ) as response:
        assert response.status_code == 404

    async with run(client.delete, f"/files/{file_id}") as response:
        assert response.status_code == 200
        data = response.json()
        assert data["filename"] == newname

    async with run(client.delete, f"/files/{file_id}") as response:
        assert response.status_code == 404


@pytest.mark.asyncio
async def test_update_md5sum(client, files):
    file_id = ""
    async with run(client.post, "/files", files=files) as response:
        assert response.status_code == 200
        data = response.json()
        file_id = data["file_id"]
        assert file_id != ""

    md5 = ""
    async with run(client.get, f"/files/{file_id}") as response:
        assert response.status_code == 200
        data = response.json()
        md5 = data["md5"]
        assert md5 != ""
        metadata = data["metadata"]
        assert "md5" not in metadata

    md5sum_id = ""
    async with run(client.put, f"/md5sum/{file_id}") as response:
        assert response.status_code == 200
        data = response.json()
        md5sum_id = data["md5sum_id"]
        assert md5sum_id != ""

    while True:
        async with run(client.get, f"/status/{md5sum_id}") as response:
            assert response.status_code == 200
            data = response.json()
            status = data["status"]
        if status == "SUCCESS":
            break

    filename = files["file"][0]
    async with run(client.get, f"/file_id/{filename}") as response:
        assert response.status_code == 200
        data = response.json()
        file_id = data["file_id"]

    async with run(client.get, f"/files/{file_id}") as response:
        assert response.status_code == 200
        data = response.json()
        metadata = data["metadata"]
        assert "md5" in metadata
        assert md5 == metadata["md5"]

    async with run(client.get, "/file_id/XXX") as response:
        assert response.status_code == 404


@pytest.fixture
def bucket():
    with main.fs_bucket() as bucket:
        return bucket


@pytest.mark.asyncio
async def test_mongodb(file, bucket):
    filename = file.name + "XXX"
    object_id = await bucket.upload_from_stream(filename, file.wrapper)
    assert str(object_id) != ""

    file_id = str(object_id)
    result = tasks.update_md5sum.delay(file_id)
    md5sum_id = result.id

    while True:
        result = celery.result.AsyncResult(md5sum_id)
        status = result.status
        assert status != "FAILURE"
        if status == "SUCCESS":
            break
