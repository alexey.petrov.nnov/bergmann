# Bergmann (тестовое задание)

## Formulation of the problem

Создать два сервиса на основе **FastAPI**.

Первый сервис (**DatabaseService**) должен служить для **CRUD** (Create, Read, Update, Delete) операций связанных с файлами, которые необходимо получать от пользователя и хранить (можно в **GridFS**, можно на диске) на сервере. На каждый загружаемый файл должен создаваться документ в **MongoDB** (использовать **Motor**) с краткой информацией о файле (кто загрузил файл, когда он это сделал, что загрузил). ID пользователей можно указывать случайные, имплементация Auth подсистемы не требуется.

Второй сервис (**FileHandlingService**) должен получать от пользователя id файла (id документа с информацией о файле в БД) и запускать **Celery** задачу (в качестве брокера можно использовать **Redis** или **RabbitMQ**), в которой производятся любые манипуляции с файлом (допустим, содержимое  дублируется и разворачивается задом наперед). Результат работы Celery таски должен быть доступен для скачивания.
Так же, следует создать несколько позитивных и негативных тестов (было бы интересно увидеть работу с фикстурами).
Сервисы необходимо запускать в Docker контейнерах. Задание лёгкое, по объему небольшое, поэтому мы ожидаем, что кандидат сконцентрируется на возможности продемонстрировать навыки структурирования программного кода и владение вышеупомянутыми технологиями. Нужно сделать не быстро, а хорошо.

## Prerequisites

- Linux or OSX
- Make (GNU)
- Docker

```shell
cd tornado
```

### Run

```shell
make up
```

### Iteractive testing

Via generated [Swagger UI](https://swagger.io/tools/swagger-ui/) web interface - [http://localhost:8000/docs](http://localhost:8000/docs)

![view in a browser](default.jpg)

### Cleaning up

```shell
make down
```

## Discussion

1. Так как **DatabaseService** и **FileHandlingService** сервисы оба используют одну и туже базу данных, то, в соответсвии с принципом изолированности микросервисов, лигически получается, что они должны быть объеденины. Подробнее можно посмотреть, например, [здесь](https://qna.habr.com/q/668554).
2. **Celery** задача состоит в в вычислении **md5sum** аттрибута для **GridFS** файла. Подробнее - [tasks.update_md5sum](https://gitlab.com/alexey.petrov.nnov/bergmann/-/blob/main/tornado/tasks.py#L36).
3. Использовался **Docker** как для запуска приложения так и тестирования. Подробнее - [docker-compose.yml](https://gitlab.com/alexey.petrov.nnov/bergmann/-/blob/main/tornado/docker-compose.yml)
4. Backend implementation located at [main.py](https://gitlab.com/alexey.petrov.nnov/bergmann/-/blob/main/tornado/main.py)
5. Corresponding unit tests located at [test_main.py](https://gitlab.com/alexey.petrov.nnov/bergmann/-/blob/main/tornado/test_main.py)
6. Code has **100% line coverage**, formatted in **black** and satisfy all **pylint** checks, [see](https://gitlab.com/alexey.petrov.nnov/bergmann/-/blob/main/tornado/Makefile#L24).
7. All other folders and files are my experiments to get the task done ;)
