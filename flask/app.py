#!/usr/bin/env python

"""Flask application that implements DatabaseService and FileHandlingService"""

import io
import os
from typing import Dict

import flask
import gridfs
from celery.result import AsyncResult
from flask.json import jsonify
from flask_pymongo import PyMongo
from gridfs.grid_file import ObjectId

from tasks import delete4mongo, update_md5sum, MONGO_URI

root = flask.Blueprint("root", __name__)


def metadata(out: gridfs.GridOut) -> Dict:
    """Constructs metadata for the given GridFS entity"""
    return dict(
        index=str(out._id),  # pylint: disable=protected-access
        aliases=out.aliases,
        content_type=out.contentType,
        length=out.length,
        md5=out.md5,
        upload_dDate=f"{out.uploadDate:%Y-%m-%d %H:%M:%S}",
    )


def save2mongo(file) -> ObjectId:
    """Saves a file obtained from the response into MongoDB"""
    content_type = file.content_type
    filename = file.filename
    delete4mongo(filename, FS)
    index = MONGO.save_file(filename, file, content_type=content_type)
    return index


@root.route("/files", methods=["POST"])
def upload_file():
    """Uploads the given file"""
    file = flask.request.files["file"]
    index = save2mongo(file)
    out = FS.get(index)
    return jsonify({file.filename: metadata(out)})


@root.route("/files", methods=["GET"])
def get_existing_files():
    """Gets list of loaded files metadata"""
    files = {}
    filenames = FS.list()
    for filename in filenames:
        out = FS.find_one({"filename": filename})
        files[filename] = metadata(out)
    return jsonify(dict(files=files))


@root.route("/files/<filename>", methods=["GET"])
def get_existing_file(filename: str):
    """Gets metadata for the loaded file with the given name"""
    out = FS.find_one({"filename": filename})
    return jsonify({filename: metadata(out)})


@root.route("/download/<filename>")
def download(filename):
    """Downloads the previously loaded file with the given name"""
    try:
        return MONGO.send_file(filename)
    except TypeError:
        out = FS.find_one({"filename": filename})
        contents = io.BytesIO(out.read())
        return flask.send_file(
            contents,
            download_name=filename,
            mimetype=out.contentType,
        )


@root.route("/files/<filename>", methods=["DELETE"])
def remove_file(filename: str):
    """Deletes the previously loaded file with the given name"""
    if not FS.exists({"filename": filename}):
        return "", 404
    count = delete4mongo(filename, FS)
    assert not FS.exists({"filename": filename})
    return jsonify(dict(count=count + 1))


@root.route("/md5sum/<filename>", methods=["GET"])
def run_md5sum(filename: str):
    """Launches MD5SUM calculation for the previously loaded file with the given name"""
    out = FS.find_one({"filename": filename})
    index = str(out._id)  # pylint: disable=protected-access
    result = update_md5sum.delay(index)
    return jsonify(dict(result_id=result.id))


@root.route("/status/<result_id>", methods=["GET"])
def status_md5sum(result_id: str):
    """Gets status on MD5SUM calculation for the given Celery result ID"""
    result = AsyncResult(result_id)
    return jsonify(dict(status=result.status))


app = flask.Flask(__name__)
storage = os.path.join(os.path.dirname(os.path.abspath(__file__)), "storage")
app.config["UPLOAD_FOLDER"] = storage
app.secret_key = "change-me"
app.register_blueprint(root)

# https://stackabuse.com/integrating-mongodb-with-flask-using-flask-pymongo/#:~:text=1%2C%0A%20%20%20%20%22ok%22%3A%201.0%0A%7D-,Saving%20and%20Retrieving%20Files,-MongoDB%20allows%20us
MONGO = PyMongo(app, MONGO_URI)
FS = gridfs.GridFS(MONGO.db)
FS.list()


def create_app():
    "Provide and access to the Flask application"
    return app
