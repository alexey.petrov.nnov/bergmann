#!/usr/bin/env python

"""Tests for Flask application"""

import io
from collections import namedtuple
from contextlib import contextmanager

import flask
import gridfs
import pytest
import werkzeug
from pymongo import MongoClient

import tasks
from app import create_app

# https://raw.githubusercontent.com/mathiasbynens/small/master/jpeg.jpg
SMALLEST_JPEG_B64 = """\
/9j/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8Q
EBEQCgwSExIQEw8QEBD/yQALCAABAAEBAREA/8wABgAQEAX/2gAIAQEAAD8A0s8g/9k=
"""
# pylint: disable=redefined-outer-name,missing-function-docstring,no-member


def test_mongodb():
    db = MongoClient(tasks.MONGO_URI)[tasks.DB_NAME]
    fs = gridfs.GridFS(db)
    fs.list()
    meta = dict(username="AAA", tags=["top-secrete"])
    obj_id = fs.put(b"hello world", metadata=meta)
    out = fs.get(obj_id)
    assert out.metadata == meta


@contextmanager
def run(engine, *args, **kwds):
    resource = engine(*args, **kwds)
    try:
        yield resource
    finally:
        pass


@pytest.fixture
def client():
    return create_app().test_client()


File = namedtuple("File", ["name", "contents", "wrapper"])


@pytest.fixture
def file() -> File:
    name = "dummy"
    contents = b"dummy"
    wrapper = werkzeug.datastructures.FileStorage(
        stream=io.BytesIO(contents), filename=name
    )
    return File(name, contents, wrapper)


def test_download(client, file: File):
    with run(
        client.post,
        "/files",
        data={"file": file.wrapper},
        content_type="multipart/form-data",
    ) as response:
        assert response.status_code == 200
        data = flask.json.loads(response.get_data(as_text=True))
        assert file.name in data
        meta = data[file.name]
    with run(client.get, f"/download/{file.name}") as response:
        data = response.get_data()
        assert data == file.contents


def test_create_and_delete_file(client, file: File):
    with run(
        client.post,
        "/files",
        data={"file": file.wrapper},
        content_type="multipart/form-data",
    ) as response:
        assert response.status_code == 200
        data = flask.json.loads(response.get_data(as_text=True))
        assert file.name in data
        meta = data[file.name]
        assert "length" in meta
        assert meta["length"] == 5
    with run(client.get, "/files") as response:
        assert response.status_code == 200
        data = flask.json.loads(response.get_data(as_text=True))
        assert "files" in data
        files = data["files"]
        assert file.name in files
        meta = files[file.name]
        assert "length" in meta
        assert meta["length"] == 5
        assert "md5" in meta
        assert meta["md5"] is None
    with run(client.get, f"/md5sum/{file.name}") as response:
        assert response.status_code == 200
        data = flask.json.loads(response.get_data(as_text=True))
        assert "result_id" in data
        result_id = data["result_id"]
        while True:
            with run(client.get, f"/status/{result_id}") as response:
                assert response.status_code == 200
                data = flask.json.loads(response.get_data(as_text=True))
                assert "status" in data
                status = data["status"]
            if status == "SUCCESS":
                break
    with run(client.get, f"/files/{file.name}") as response:
        assert response.status_code == 200
        data = flask.json.loads(response.get_data(as_text=True))
        assert file.name in data
        meta = data[file.name]
        assert "length" in meta
        assert meta["length"] == 5
        assert "md5" in meta
        assert meta["md5"] is not None
    with run(client.delete, f"/files/{file.name}") as response:
        assert response.status_code == 200
        data = flask.json.loads(response.get_data(as_text=True))
        assert data["count"] != 0
    with run(client.delete, f"/files/{file.name}") as response:
        assert response.status_code == 404
