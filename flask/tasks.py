#!/usr/bin/env python3

"""Celery application and tasks definition"""

import hashlib

import gridfs
from celery import Celery
from gridfs.grid_file import ObjectId
from pymongo import MongoClient

DB_NAME = "test-database"
# https://docs.mongodb.com/manual/reference/connection-string/
MONGO_URI = f"mongodb://test-user:test-password@mongodb-srv:27017/{DB_NAME}"

app = Celery("tasks", backend="redis://redis-srv:6379", broker="redis://redis-srv:6379")


def grid() -> gridfs.GridFS:
    """Gets MongoClient for the defined environment"""
    db = MongoClient(MONGO_URI)[DB_NAME]
    return gridfs.GridFS(db)


def delete4mongo(filename: str, engine: gridfs.GridFS) -> int:
    """Deletes all the files with the given name from MongoDB
    and returns how much"""
    count = 1
    for grid_out in engine.find({"filename": filename}):
        engine.delete(grid_out._id)  # pylint: disable=protected-access
        count += 1
    return count


@app.task
def update_md5sum(file_id: str) -> str:
    """Calculates MD5SUM for the GridFS file with the given ID
    and stores it with the updated metadata"""
    fs = grid()
    out = fs.get(ObjectId(file_id))
    contents = out.read()
    md5 = hashlib.md5(contents).hexdigest()
    delete4mongo(out.filename, fs)
    file_id = fs.put(
        contents,
        filename=out.filename,
        content_type=out.content_type,
        metadata=out.metadata,
        md5=md5,
    )

    return str(file_id)
