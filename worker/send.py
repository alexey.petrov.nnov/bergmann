#!/usr/bin/env python3
import pika
import sys

connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq-srv"))
channel = connection.channel()

channel.exchange_declare(exchange="logs", exchange_type="fanout")

message = " ".join(sys.argv[1:]) or "Hello World!"
for i in range(10):
    body = f"[{i}] - " + message
    channel.basic_publish(exchange="logs", routing_key="", body=body)
    print("Sent: %r" % body)
connection.close()
